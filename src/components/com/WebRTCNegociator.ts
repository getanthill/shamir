import type { Datastore } from '@getanthill/datastore';

import { v4 as uuidv4 } from 'uuid';
import { EventEmitter } from 'events';

interface ICEServer {
	username?: string;
	credential?: string;
	url: string;
	urls: string;
}

// WebRTC Connector configuration
interface WebRTCNegociatorConfig {
	id: string;
	iceServers?: ICEServer[];
	iceServersFetchUrl: string | null;
	iceServersSyncTTLThreshold: number;
}

export interface ConnectionStatus {
	connecting: boolean;
	offering: boolean;
	disconnecting: boolean;
	iceservices: boolean;
	ready: boolean;
	error: boolean;
}

export default class WebRTCNegociator extends EventEmitter {
	config: WebRTCNegociatorConfig = {
		id: 'john',
		iceServers: [],
		iceServersFetchUrl: null,
		iceServersSyncTTLThreshold: 0.9
	};

	ds: Datastore;

	iceServersSetTimeout: NodeJS.Timeout | undefined;

	private RTCConfiguration: RTCConfiguration = {};
	status = new Map<string, ConnectionStatus>();
	dataChannels = new Map<string, RTCDataChannel>();
	peerConnections = new Map<string, RTCPeerConnection>();
	iceCandidates = new Map<string, any>();
	attempts: { [key: string]: number } = {};

	private offerOptions: RTCOfferOptions = {
		/**
		 * @warning Deprecation
		 * @see https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/createOffer#RTCOfferOptions_dictionary
		 * @see https://developer.mozilla.org/en-US/docs/Web/API/RTCRtpTransceiver
		 */
		offerToReceiveVideo: false,
		offerToReceiveAudio: false
	};

	makingOffer = new Map<string, boolean>();
	ignoreOffer = new Map<string, boolean>();

	constructor(config: Partial<WebRTCNegociatorConfig>, ds: Datastore) {
		super();

		this.config = {
			...this.config,
			...config
		};

		this.ds = ds;
	}

	peerConnection(uuid: string, forceNew = false): RTCPeerConnection {
		if (this.peerConnections.has(uuid)) {
			if (forceNew === true) {
				return this.close(uuid, false).peerConnection(uuid);
			}

			console.log('webrtc peer connection already created...', { uuid });
			return this.peerConnections.get(uuid)!;
		}

		const pc = new RTCPeerConnection({
			...this.RTCConfiguration,
			iceServers: this.config.iceServers
		});

		pc.onnegotiationneeded = async () => {
			try {
				console.log('webrtc onnegotiationneeded with ', uuid);
				this.makingOffer.set(uuid, true);
				await pc.setLocalDescription();
				this.sendSignal(uuid, 'description', { description: pc.localDescription });
			} catch (err) {
				console.error(err);
			} finally {
				this.makingOffer.delete(uuid);
			}
		};

		this.iceCandidates.set(uuid, []);
		pc.onicecandidate = ({ candidate }) => {
			if (candidate) {
				this.sendSignal(uuid, 'candidate', { candidate });

				this.iceCandidates.set(uuid, [...(this.iceCandidates.get(uuid) ?? []), candidate]);
			}
		};

		const dataChannel = pc.createDataChannel('messageChannel', {
			negotiated: true,
			id: 0
		});

		dataChannel.onopen = () => {
			console.log('webrtc Data channel open');
			this.emit('ready', { to: uuid, channel: dataChannel });

			this.makingOffer.delete(uuid);
		};

		dataChannel.onclose = () => {
			this.close(uuid);
		};

		this.dataChannels.set(uuid, dataChannel);

		this.peerConnections.set(uuid, pc);

		return pc;
	}

	async onSignal({ candidate, description, from, to, type }: any) {
		try {
			if (description) {
				this.attempts[from] = (this.attempts[from] ?? 0) + 1;

				if (this.attempts[from] > 10) {
					return this.close(from);
				}

				const isOffer = description.type === 'offer';
				const pc = this.peerConnection(from);

				if (isOffer === false && pc.localDescription) {
					/**
					 * I already sent an offer and this is the answer
					 */
					await pc.setRemoteDescription(description);

					return;
				}

				if (isOffer === true && pc.localDescription) {
					/**
					 * I am receving the offer and I am not in an offering
					 * state: I can answer to the offer.
					 */
					await Promise.all([
						pc.setRemoteDescription(description),
						pc.setLocalDescription({ type: 'rollback' })
					]);

					const answer = await pc.createAnswer();
					await pc.setLocalDescription(answer);

					return;
				}

				if (isOffer === true && !pc.localDescription) {
					/**
					 * I am receving the offer and I am not in an offering
					 * state: I can answer to the offer.
					 */
					await pc.setRemoteDescription(description);

					const answer = await pc.createAnswer();
					await pc.setLocalDescription(answer);
					this.sendSignal(from, 'description', {
						description: pc.localDescription
					});

					return;
				}
			} else if (candidate) {
				const pc = this.peerConnection(from);
				try {
					// const pc = this.peerConnection(to);

					console.log('[webrtc] Adding ICE Candidate');
					await pc.addIceCandidate(candidate);
				} catch (err) {
					if (this.ignoreOffer.get(to) !== true) {
						throw err;
					}
				}
			}
		} catch (err) {
			console.error(err);
		}
	}

	sendSignal(uuid: string, type: string, message: any) {
		this.emit('log', 'Sending signal', { type });
		this.ds.apply('messages', uuidv4(), 'MESSAGE_CREATED', '1.0.0', {
			conversation_id: `webrtc://${uuid}`,
			author_id: this.config.id,
			message: JSON.stringify({ type, to: uuid, from: this.config.id, ...message }),
			expired_by: new Date(Date.now() + 60 * 1000).toISOString()
		});
	}

	close(uuid: string, mustEmit = true): this {
		this.emit('log', 'Closing...', { uuid });

		const pc = this.peerConnections.get(uuid);

		if (!pc) {
			return this;
		}

		pc.close();
		this.dataChannels.delete(uuid);
		this.peerConnections.delete(uuid);
		this.makingOffer.delete(uuid);
		this.iceCandidates.delete(uuid);

		mustEmit === true && this.emit('close', { uuid });

		return this;
	}

	async connect(uuid: string) {
		const pc = this.peerConnection(uuid, true);

		console.log('[webrtc] Creating the offer');
		const offer = await pc.createOffer(this.offerOptions);

		console.log('[webrtc] Setting local description');
		await pc.setLocalDescription(offer);

		console.log('[webrtc] Sending the local description signal');

		this.sendSignal(uuid, 'description', { description: pc.localDescription });
	}
}
