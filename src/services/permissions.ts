
export async function handlePermission(name: PermissionName) {
	return navigator.permissions.query({ name });
}
