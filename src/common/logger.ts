import type { LoggerOptions } from 'winston';

export type LoggerFn = (message: string, context?: any) => void;

export interface Logger {
	trace: LoggerFn;
	debug: LoggerFn;
	info: LoggerFn;
	warn: LoggerFn;
	error: LoggerFn;
	critical: LoggerFn;
	fatal: LoggerFn;
}

const LEVELS: { [key: string]: Function } = {
	trace: console.trace,
	debug: console.debug,
	info: console.info,
	warn: console.warn,
	error: console.error,
	critical: console.error,
	fatal: console.error
};
const LEVEL_INDEXES: { [key: string]: number } = {
	trace: 0,
	debug: 1,
	info: 2,
	warn: 3,
	error: 4,
	critical: 5,
	fatal: 6
};
let level = 'info';
let levelIndex = LEVEL_INDEXES[level];

function log(level: string, message: string, context?: any) {
	if (LEVEL_INDEXES[level] < levelIndex) {
		return;
	}

	LEVELS[level](level, message, context);
}

export function build(opts?: LoggerOptions): Logger {
	level = opts?.level ?? 'info';
	levelIndex = LEVEL_INDEXES[level];

	return {
		trace: log.bind(null, 'trace'),
		debug: log.bind(null, 'debug'),
		info: log.bind(null, 'info'),
		warn: log.bind(null, 'warn'),
		error: log.bind(null, 'error'),
		critical: log.bind(null, 'critical'),
		fatal: log.bind(null, 'fatal')
	};
}
