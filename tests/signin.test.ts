import { expect, test, type Dialog } from '@playwright/test';

test.beforeEach(async ({ page }) => {
	await page.goto(
		`/signin?ds=${process.env.DS_URL || 'http://localhost:3001'}&token=${process.env.DS_TOKEN || 'token'}`,
		{}
	);

	await page.waitForSelector('#email', { state: 'visible' });
});

test('signin is possible with an already available account', async ({ page }) => {
	await page.locator('#email').fill('john@doe.org');
	await page.locator('#password').fill('xxx');

	await expect(page.locator('button[type=submit]')).toBeEnabled();

	page.on('dialog', (dialog) => dialog.accept('0000'));
	await page.locator('button[type=submit]').click();

	await page.waitForSelector('#message', { state: 'visible' });

	await expect(await page.getByText('Hi!')).toHaveCount(1);
});

test('blocks signin if OTP is empty', async ({ page }) => {
	await page.locator('#email').fill('john@doe.org');
	await page.locator('#password').fill('xxx');

	await expect(page.locator('button[type=submit]')).toBeEnabled();

	let onDialog = (dialog: Dialog) => dialog.accept('');
	page.on('dialog', onDialog);
	await page.locator('button[type=submit]').click();

	await expect(await await page.locator('button[type=submit]')).toHaveCount(1);
});

test('blocks signin if OTP prompt is dismissed', async ({ page }) => {
	await page.locator('#email').fill('john@doe.org');
	await page.locator('#password').fill('xxx');

	await expect(page.locator('button[type=submit]')).toBeEnabled();

	let onDialog = (dialog: Dialog) => dialog.dismiss();
	page.on('dialog', onDialog);
	await page.locator('button[type=submit]').click();

	await expect(await await page.locator('button[type=submit]')).toHaveCount(1);
});

test('refresh after signin allows to restore the session', async ({ page }) => {
	await page.locator('#email').fill('john@doe.org');
	await page.locator('#password').fill('xxx');

	await expect(page.locator('button[type=submit]')).toBeEnabled();

	page.on('dialog', (dialog) => dialog.accept('0000'));
	await page.locator('button[type=submit]').click();

	await page.waitForSelector('#message', { state: 'visible' });

	await expect(await page.getByText('Hi!')).toHaveCount(1);

	await page.reload();

	await page.waitForSelector('#message', { state: 'visible' });

	await expect(await page.getByText('Hi!')).toHaveCount(1);
});

test('blocks reauth after signin if OTP is incorrect', async ({ page }) => {
	await page.locator('#email').fill('john@doe.org');
	await page.locator('#password').fill('xxx');

	await expect(page.locator('button[type=submit]')).toBeEnabled();

	let onDialog = (dialog: Dialog) => dialog.accept('0000');
	page.on('dialog', onDialog);
	await page.locator('button[type=submit]').click();

	await page.waitForSelector('#message', { state: 'visible' });

	await expect(await page.getByText('Hi!')).toHaveCount(1);

	page.off('dialog', onDialog);
	page.on('dialog', (dialog) => dialog.accept('1111'));
	await page.reload();

	await expect(await await page.locator('button[type=submit]')).toHaveCount(1);
});
