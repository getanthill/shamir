import type { Services } from './services';

export async function bind(services: Services) {
	services.telemetry.logger.info('[api#bind] Binding API routes');

	services.communication.on('get', get);
	services.communication.on('find', find);
	services.communication.on('insert', insert);
	services.communication.on('update', update);
	services.communication.on('patch', patch);
	services.communication.on('remove', remove);
}

async function get(services: Services, event: MessageEvent) {
	try {
		const result = await services.data.get(event.data.store, event.data.correlation_id);

		services.communication.success(services, event, result);
	} catch (err) {
		services.communication.error(services, event, err);
	}
}

async function find(services: Services, event: MessageEvent) {
	try {
		const result = await services.data.find(event.data.store, event.data.query);

		services.communication.success(services, event, result);
	} catch (err) {
		services.communication.error(services, event, err);
	}
}

async function insert(services: Services, event: MessageEvent) {
	try {
		const result = await services.data.insert(event.data.store, event.data.entity);

		services.communication.success(services, event, result);
	} catch (err) {
		services.communication.error(services, event, err);
	}
}

async function update(services: Services, event: MessageEvent) {
	try {
		const result = await services.data.update(event.data.store, event.data.entity);

		services.communication.success(services, event, result);
	} catch (err) {
		services.communication.error(services, event, err);
	}
}

async function patch(services: Services, event: MessageEvent) {
	try {
		const result = await services.data.patch(event.data.store, event.data.entity);

		services.communication.success(services, event, result);
	} catch (err) {
		services.communication.error(services, event, err);
	}
}

async function remove(services: Services, event: MessageEvent) {
	try {
		const result = await services.data.remove(event.data.store, event.data.correlation_id);

		services.communication.success(services, event, result);
	} catch (err) {
		services.communication.error(services, event, err);
	}
}
