import type { Services } from '.';

export function share(services: Services, url: string, title: string, text: string) {
	if (!navigator.share) {
		throw new Error('share API unavailable');
	}

	services.telemetry.logger.info('[shares] Sharing link', {
		url,
		title,
		text
	});

	navigator.share({ url, text, title });
}
