/// <reference types="@sveltejs/kit" />
import { build, files, version } from '$service-worker';

import services from './services';

import * as api from './api';
import * as push from './push';

// Create a unique cache name for this deployment
const CACHE = `cache-${version}`;

const ASSETS = [
	...build, // the app itself
	...files // everything in `static`
];

console.log('[sw] Cache', CACHE);
console.log('[sw] Assets', ASSETS);

self.addEventListener('install', (event) => {
	console.log('[sw] Install');
	self.skipWaiting();
	// Create a new cache and add all files to it
	async function addFilesToCache() {
		const cache = await caches.open(CACHE);
		await cache.addAll(ASSETS);
	}

	event.waitUntil(Promise.all([addFilesToCache()]));
});

console.log('[sw] loaded');

self.addEventListener('activate', (event) => {
	console.log('[sw] Activate');
	self.skipWaiting();
	// Remove previous cached data from disk
	async function deleteOldCaches() {
		for (const key of await caches.keys()) {
			if (key !== CACHE) await caches.delete(key);
		}
	}

	event.waitUntil(Promise.all([deleteOldCaches(), self.clients.claim(), main()]));
});

self.addEventListener('message', (event: MessageEvent) => {
	if (event.data.type === 'init') {
		services.telemetry.logger.info('[sw#init] Initializing message received');
		return main();
	}
});

let isInitialized = false;
async function main() {
	if (isInitialized === true) {
		services.telemetry.logger.info('[services#init] Already initialized');
		return;
	}

	services.telemetry.logger.info('[services#init] Initializing services');

	services.telemetry.logger.debug('[services#init] Listening on new messages');

	await Promise.all([api.bind(services), push.bind(services)]);

	await services.communication.bind(services);

	isInitialized = true;
}
