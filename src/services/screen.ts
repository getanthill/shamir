import { get } from 'svelte/store';

import * as stores from './stores';

export async function lock() {
	const currentLock = get(stores.screenLock);

	if (currentLock !== null) {
		return currentLock;
	}

	const screenLock = await navigator.wakeLock.request();

	stores.screenLock.set(screenLock);

	return screenLock;
}

export async function release() {
	const currentLock = get(stores.screenLock);

	if (currentLock === null) {
		return currentLock;
	}

	currentLock?.release();
	stores.screenLock.set(null);
}
