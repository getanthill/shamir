import { v4 as uuidv4 } from 'uuid';

const handlers = new Map<string, Function>();

let isReady = false;

function onMessage(event: MessageEvent) {
	const callId = event.data.call_id;
	if (callId && handlers.has(callId)) {
		const handler = handlers.get(callId)!;
		handlers.delete(callId);

		return handler(event);
	}

	console.log('[communications#onMessage] Message received', event.data);
}

export function bind() {
	navigator.serviceWorker.addEventListener('message', onMessage);
}

export function unbind() {
	navigator.serviceWorker.removeEventListener('message', onMessage);
}

export function setReady(ready: boolean) {
	isReady = ready;
}

export async function waitForRegistrations(): Promise<void> {
	if (isReady === true) {
		return Promise.resolve();
	}

	await navigator.serviceWorker.ready;

	return new Promise((resolve) => {
		const wait = async () => {
			const registrations = await navigator.serviceWorker.getRegistrations();

			if (registrations.length > 0 && navigator.serviceWorker.controller) {
				setReady(true);
				resolve();

				return;
			}

			setTimeout(wait, 10);
		};

		wait();
	});
}

export async function send(message: any) {
	await waitForRegistrations();

	navigator.serviceWorker.controller.postMessage(message);
}

export async function heartbeat() {
	return callWithRetry(
		{
			type: 'heartbeat'
		},
		100,
		100
	);
}

export async function callWithRetry(
	message: any,
	retry = 1,
	timeout = 1000
): Promise<{ type: string; value: any }> {
	let error;
	for (let i = 0; i < retry; i++) {
		try {
			let result = await call(message, timeout);

			return result;
		} catch (err) {
			error = err;
		}
	}

	throw error;
}

export async function call(message: any, timeout = 1000): Promise<{ type: string; value: any }> {
	const callId = uuidv4();
	return new Promise((resolve, reject) => {
		const _timout = setTimeout(() => {
			reject(new Error('[communications#call] Timeout'));
		}, timeout);

		const onResponse = (event: MessageEvent) => {
			clearTimeout(_timout);
			if (event.data.type.endsWith('/error')) {
				return reject(event.data.error);
			}

			resolve(event.data);
		};

		handlers.set(callId, onResponse);

		send({ ...message, call_id: callId });
	});
}
