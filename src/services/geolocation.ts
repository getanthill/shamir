import { get } from 'svelte/store';

import * as stores from './stores';

const DEFAULT_GEOLOCATION_OPTIONS = {
	enableHighAccuracy: true,
	maximumAge: 0,
	timeout: 5000
};

export async function getCurrentPosition(): Promise<GeolocationPosition> {
	return new Promise((resolve, reject) => {
		if (!navigator.geolocation) {
			return reject(new Error('Geolocation API is unavailable'));
		}

		navigator.geolocation.getCurrentPosition(
			(position: GeolocationPosition) => {
				stores.location.set(position);
				resolve(position);
			},
			reject,
			DEFAULT_GEOLOCATION_OPTIONS
		);
	});
}

export async function currentPositionUpdated(position: GeolocationPosition): Promise<void> {
	stores.location.set(position);
}

export function watchPosition() {
	const currentWatchPositionId = get(stores.watchPositionId);

	if (currentWatchPositionId !== null) {
		return currentWatchPositionId;
	}

	console.log('[geolocation] Watching position');
	const watchPositionId = navigator.geolocation.watchPosition(
		(position) => {
			console.log('[geolocation] Position updated', { position });
			stores.location.set(position);
		},
		(err) => {
			console.error(err);
		},
		DEFAULT_GEOLOCATION_OPTIONS
	);

	stores.watchPositionId.set(watchPositionId);

	return watchPositionId;
}

export function clearWatchPosition(): void {
	console.log('[geolocation] Stop watching position');
	const currentWatchPositionId = get(stores.watchPositionId);

	currentWatchPositionId && navigator.geolocation.clearWatch(currentWatchPositionId);

	stores.watchPositionId.set(null);
}
