module.exports = {
	ci: {
		collect: {
			numberOfRuns: 1,
			startServerCommand: 'npm run preview',
			url: ['http://localhost:5173', 'http://localhost:5173/signin'],
			settings: {
				onlyCategories: ['performance', 'accessibility', 'best-practices', 'seo', 'pwa'],
				skipAudits: ['uses-http2'],
				chromeFlags: '--no-sandbox',
				extraHeaders: JSON.stringify({
					Cookie: 'customCookie=1;foo=bar'
				})
			},
			assert: {
				assertions: {
					'categories:performance': ['error', { minScore: 0.1, aggregationMethod: 'median-run' }],
					'categories:accessibility': [
						'error',
						{ minScore: 0.1, aggregationMethod: 'pessimistic' }
					],
					'categories:best-practices': [
						'error',
						{ minScore: 0.1, aggregationMethod: 'pessimistic' }
					],
					'categories:seo': ['error', { minScore: 0.1, aggregationMethod: 'pessimistic' }]
				}
			},
			upload: {
				target: 'temporary-public-storage'
			}
		}
	}
};
