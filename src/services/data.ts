import { call } from './communications';

/**
 * @see https://web.dev/learn/pwa/offline-data
 */
export async function requestPermission() {
	if (navigator.storage && navigator.storage.persist) {
		await navigator.storage.persist();
	}
}

export async function g(key: string, defaultValue: any = null) {
	const searchParams = new URLSearchParams(window.location.search);

	if (searchParams.has(key)) {
		return searchParams.get(key);
	}

	let d = await get('local', key);
	if (d) {
		return d.value;
	}

	return JSON.parse(
		sessionStorage.getItem(key) ?? localStorage.getItem(key) ?? JSON.stringify(defaultValue)
	);
}

export async function s(key: string, value: any) {
	return update('local', {
		name: key,
		value
	});
}

export async function insert(storeName: string, payload: any) {
	const res = await call({
		type: 'insert',
		store: storeName,
		entity: payload
	});

	return res.value;
}

export async function update(storeName: string, payload: any) {
	const res = await call({
		type: 'update',
		store: storeName,
		entity: payload
	});

	return res.value;
}

export async function patch(storeName: string, payload: any) {
	const res = await call({
		type: 'patch',
		store: storeName,
		entity: payload
	});

	return res.value;
}

export async function get(storeName: string, correlationId: string) {
	const res = await call({
		type: 'get',
		store: storeName,
		correlation_id: correlationId
	});

	return res.value;
}

export async function find(storeName: string) {
	const res = await call({
		type: 'find',
		store: storeName
	});

	return res.value;
}

export async function remove(storeName: string, correlationId: string) {
	const res = await call({
		type: 'remove',
		store: storeName,
		correlation_id: correlationId
	});

	return res.value;
}
