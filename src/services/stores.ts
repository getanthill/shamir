import type { ModelConfig } from '@getanthill/datastore/dist/typings';
import type { Datastore } from '@getanthill/datastore';
import type { AuthResponse } from '../types';

import { derived, readable, writable } from 'svelte/store';

import { getCurrentBrowserFingerPrint } from '@rajesh896/broprint.js';
import * as h3 from 'h3-js';

import { getCurrentPosition } from './geolocation';

export const models = writable<null | Record<string, ModelConfig>>(null);
export const datastore = writable<null | Datastore>(null);

export const logs = writable<Array<string>>([]);

export const auth = writable<AuthResponse>({
	tokens: {},
	account: null
});

/**
 * OTP used to encrypt safely the authentication tokens
 */
export const otp = writable<string>('', (set) => {
	set(sessionStorage.getItem('otp') ?? '');
});

/**
 * OTP is requested
 */
export const requestOtp = writable<boolean>(false);

/**
 * Get the device fingerprint to store tokens in the localStorage.
 */
export const fingerprint = writable<string | null>(null, (set) => {
	if (typeof window === 'undefined') {
		return () => null;
	}

	getCurrentBrowserFingerPrint().then((fingerprint) => {
		console.log('Fingerprint', fingerprint);
		set(`${fingerprint}`);
	});

	return () => null;
});

/**
 * Network status monitoring
 */
export const network = readable<{
	is_online: boolean;
	is_back: boolean;
	updated_at: Date;
}>(
	{
		is_online: true,
		is_back: false,
		updated_at: new Date()
	},
	(_set, update) => {
		if (typeof window === 'undefined') {
			return () => null;
		}

		const onChange = () =>
			update((previous) => ({
				is_online: navigator.onLine,
				is_back: previous.is_online === false,
				updated_at: new Date()
			}));

		window.addEventListener('offline', onChange);
		window.addEventListener('online', onChange);

		return () => {
			window.removeEventListener('offline', onChange);
			window.removeEventListener('online', onChange);
		};
	}
);

/**
 * `ping` value in milliseconds to the datastore
 */
let tickInterval: NodeJS.Timeout;
export const ping = derived(
	[datastore, network],
	([$datastore, $network], set) => {
		if ($datastore === null || $network.is_online === false) {
			clearInterval(tickInterval);

			set(-1);

			return;
		}

		const setPing = async () => {
			const tic = Date.now();
			await fetch(`${$datastore.config.baseUrl}/heartbeat`);

			set(Date.now() - tic);
		};

		setPing();
		tickInterval = setInterval(setPing, 10_000); // Every 10 seconds

		return () => {
			clearInterval(tickInterval);
		};
	},
	-1
);

/**
 * Geolocation
 */
export const watchPositionId = writable<number | null>(null);
export const location = writable<GeolocationPosition | null>(null, (set) => {
	if (typeof window === 'undefined') {
		return () => null;
	}

	getCurrentPosition()
		.then((position) => {
			console.log('[stores] Location retrieved');
			set(position);
		})
		.catch((err): void => {
			console.warn('[stores] Failed retrieving position', err);
		});
});
export const cell = derived(location, ($location: GeolocationPosition | null) => {
	if ($location === null) {
		return null;
	}

	const accuracy = $location.coords.accuracy;
	const accuracyArea = Math.PI * accuracy * accuracy;
	let resolution = 15;
	for (let i = resolution; i > 0; i--) {
		const area = h3.getHexagonAreaAvg(i, h3.UNITS.m2);

		if (area > accuracyArea) {
			resolution = Math.min(15, i + 1);
			// console.log('[h3] Resolution found', {
			// 	resolution,
			// 	area: h3.getHexagonAreaAvg(resolution, h3.UNITS.m2),
			// 	accuracyArea,
			// 	accuracy
			// });
			break;
		}
	}

	return h3.latLngToCell($location.coords.latitude, $location.coords.longitude, resolution);
});

/**
 * Network status monitoring
 */
export const visibility = readable<{
	is_visible: boolean;
	is_back: boolean;
	updated_at: Date;
}>(
	{
		is_visible: true,
		is_back: false,
		updated_at: new Date()
	},
	(_set, update) => {
		if (typeof window === 'undefined') {
			return () => null;
		}

		const onChange = () =>
			update((previous) => ({
				is_visible: document.visibilityState === 'visible',
				is_back: previous.is_visible === false,
				updated_at: new Date()
			}));

		window.addEventListener('visibilitychange', onChange);

		return () => {
			window.removeEventListener('visibilitychange', onChange);
		};
	}
);

/**
 * OTP is requested
 */
export const screenLock = writable<WakeLockSentinel | null>(null);

/**
 * Push notification subscription
 */
export const pushSubscription = writable<null | {
	endpoint: string;
	p256dh: string;
	auth: string;
}>(null);
