import * as sec from '@getanthill/sec';

import buildServices, { type Services, init } from '../services';
import * as usecases from './usecases';
import * as stores from '../services/stores';

let services: Services = buildServices();

export default {
	services,
	init,
	// ---
	stores,
	// ---
	accounts: sec.usecases.accounts,
	data: sec.usecases.data,
	geolocation: usecases.geolocation,
	messages: sec.usecases.messages,
	conversations: usecases.conversations,
	authentication: usecases.authentication
};
