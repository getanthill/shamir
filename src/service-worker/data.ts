const DATABASE_NAME = 'shamir';
const DATABASE_VERSION = 6;

let db: IDBDatabase | null = null;

const getDb = connect();

export async function initDatabase(db: IDBDatabase) {
	console.log('[data#connect] Initializing local database', {
		name: DATABASE_NAME,
		version: DATABASE_VERSION,
		current: db.version
	});

	if (!db.objectStoreNames.contains('local')) {
		db.createObjectStore('local', { keyPath: 'name' });
	}

	return db;
}

export async function connect(): Promise<IDBDatabase> {
	if (db !== null) {
		console.log('[data#connect] Database already initialized');
		return Promise.resolve(db);
	}

	return new Promise((resolve, reject) => {
		console.log('[data#connect] Initializing local database', {
			name: DATABASE_NAME,
			version: DATABASE_VERSION
		});
		const request = indexedDB.open(DATABASE_NAME, DATABASE_VERSION);
		request.onerror = reject;
		request.onupgradeneeded = async () => {
			console.log('[data#connect] Database upgrade required', {
				version: DATABASE_VERSION
			});
			const db = await initDatabase(request.result);

			resolve(db);
		};

		request.onsuccess = async () => {
			console.log('[data#connect] Database connection established');

			resolve(request.result);
		};
	});
}

export async function insert(storeName: string, payload: any) {
	const db = await getDb;
	const transaction = db.transaction([storeName], 'readwrite');

	return new Promise((resolve, reject) => {
		transaction.oncomplete = (event) => {
			resolve(payload);
		};

		transaction.onerror = (event) => {
			reject(transaction.error);
		};

		const objectStore = transaction.objectStore(storeName);

		objectStore.add(payload);
	});
}

export async function update(storeName: string, payload: any) {
	const db = await getDb;
	const transaction = db.transaction([storeName], 'readwrite');

	return new Promise((resolve, reject) => {
		transaction.oncomplete = (event) => {
			resolve(payload);
		};

		transaction.onerror = (event) => {
			reject(transaction.error);
		};

		const objectStore = transaction.objectStore(storeName);

		objectStore.put(payload);
	});
}

export async function patch(storeName: string, payload: any) {
	const db = await getDb;
	const transaction = db.transaction([storeName], 'readwrite');
	const objectStore = transaction.objectStore(storeName);
	const correlationId = payload[objectStore.keyPath as string];

	return new Promise((resolve, reject) => {
		const request = objectStore.get(correlationId);
		request.onerror = (event) => {
			reject(request.error);
		};
		request.onsuccess = (event) => {
			// Get the old value that we want to update
			const data = request.result;

			// update the value(s) in the object that you want to change
			for (const property in payload) {
				if (objectStore.keyPath === property) {
					continue;
				}

				data[property] = payload[property];
			}

			// Put this updated object back into the database.
			const requestUpdate = objectStore.put(data);
			requestUpdate.onerror = (event) => {
				reject(requestUpdate.error);
			};
			requestUpdate.onsuccess = (event) => {
				resolve(data);
			};
		};
	});
}

export async function get(storeName: string, correlationId: string) {
	const db = await getDb;
	const transaction = db.transaction([storeName], 'readonly');
	const objectStore = transaction.objectStore(storeName);

	return new Promise((resolve, reject) => {
		const request = objectStore.get(correlationId);
		request.onerror = (event) => {
			reject(request.error);
		};
		request.onsuccess = () => {
			resolve(request.result);
		};
	});
}

export async function find(storeName: string, query: any = {}): Promise<Array<any>> {
	const db = await getDb;
	const transaction = db.transaction([storeName], 'readonly');
	const objectStore = transaction.objectStore(storeName);
	const request = objectStore.openCursor();

	return new Promise((resolve, reject) => {
		const entities: Array<any> = [];

		request.onsuccess = (event) => {
			const cursor = request.result;
			if (cursor) {
				entities.push(cursor.value);
				cursor.continue();
			} else {
				resolve(entities);
			}
		};
	});
}

export async function remove(storeName: string, correlationId: string): Promise<void> {
	const db = await getDb;
	const transaction = db.transaction([storeName], 'readwrite');

	return new Promise((resolve, reject) => {
		transaction.oncomplete = (event) => {
			resolve();
		};

		transaction.onerror = (event) => {
			reject(transaction.error);
		};

		const objectStore = transaction.objectStore(storeName);

		objectStore.delete(correlationId);
	});
}

export async function g(key: string, defaultValue: any = null) {
	let d = await get('local', key);
	if (d) {
		return d.value;
	}

	return JSON.parse(
		sessionStorage.getItem(key) ?? localStorage.getItem(key) ?? JSON.stringify(defaultValue)
	);
}

export async function s(key: string, value: any) {
	return update('local', {
		name: key,
		value
	});
}
