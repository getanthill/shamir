import { v4 as uuidv4 } from 'uuid';
import * as sec from '@getanthill/sec';
import type {
	Conversation,
	ConversationCreated,
	Membership,
	MembershipCreated,
	Message,
	Services
} from '../../types';

export async function create(
	services: Services,
	ownerId: string,
	accountPublicKey: string,
	conversationId: string = uuidv4()
): Promise<Conversation> {
	const conversationKeyPair = await sec.usecases.crypto.generateKeyPair();

	const encryptedConversationPrivateKey = await sec.usecases.data.encryptWithPublicKey(
		accountPublicKey,
		conversationKeyPair.privateKey
	);

	return services.models.conversations.apply(
		'CONVERSATION_CREATED',
		{
			owner_id: ownerId,
			public_key: conversationKeyPair.publicKey,
			private_key: encryptedConversationPrivateKey,
			conversation_id: conversationId
		} as ConversationCreated,
		{},
		'1.0.0'
	);
}

export async function invite(
	services: Services,
	encryptedConversationPrivateKey: string,
	accountPrivateKey: string,
	conversationId: string,
	accountId: string
): Promise<Membership> {
	const {
		data: [account]
	} = await services.datastores.models.find(
		'accounts',
		{
			account_id: accountId
		},
		0,
		1,
		{
			// @ts-ignore
			decrypt: 'true'
		}
	);

	const conversationPrivateKey = await sec.usecases.data.decryptWithPrivateKey(
		accountPrivateKey,
		encryptedConversationPrivateKey
	);

	const accountEncryptedPrivateKey = await sec.usecases.data.encryptWithPublicKey(
		account.public_key,
		conversationPrivateKey
	);

	const [membership] = await services.models.memberships.find({
		conversation_id: conversationId,
		account_id: accountId
	});

	if (membership) {
		return membership;
	}

	return services.models.memberships.apply(
		'MEMBERSHIP_CREATED',
		{
			conversation_id: conversationId,
			account_id: accountId,
			private_key: accountEncryptedPrivateKey,
			membership_id: uuidv4()
		} as MembershipCreated,
		{},
		'1.0.0'
	);
}

export async function send(
	services: Services,
	conversation: Conversation,
	authorId: string,
	message: string
): Promise<Message> {
	const encryptedMessage = await sec.usecases.data.encryptWithPublicKey(
		conversation.public_key,
		message
	);

	return services.models.messages.apply(
		'MESSAGE_CREATED',
		{
			conversation_id: conversation.conversation_id,
			author_id: authorId,
			message: encryptedMessage,
			message_id: uuidv4()
		},
		{},
		'1.0.0'
	);
}

export async function read(
	services: Services,
	membership: Membership,
	privateKey: string,
	message: string
): Promise<string> {
	const conversationPrivateKey = await sec.usecases.data.decryptWithPrivateKey(
		privateKey,
		membership.private_key
	);

	return sec.usecases.data.decryptWithPrivateKey(conversationPrivateKey, message);
}

export default {
	create,
	invite,
	read,
	send
};
