import type { Services } from './services';

import EventEmitter from 'events';

export default class Communication extends EventEmitter {
	constructor() {
		super();
	}

	bind(services: Services) {
		services.telemetry.logger.info('[com#bind] Bind event listeners ');
		self.addEventListener('message', this.onMessage.bind(this, services));
	}

	async broadcast(services: Services, event: MessageEvent, message: any) {
		const clients = await self.clients.matchAll({
			includeUncontrolled: true
		});

		clients.forEach((client: MessageEventSource) => this.send(services, event, message, client));
	}

	async send(
		services: Services,
		event: MessageEvent,
		message: any,
		client?: MessageEventSource | null
	) {
		if (!client) {
			return this.broadcast(services, event, message);
		}

		client.postMessage(message);
	}

	async success(
		services: Services,
		event: MessageEvent,
		value: any,
		client?: MessageEventSource | null
	) {
		return this.send(
			services,
			event,
			{
				type: `${event.data.type}/success`,
				call_id: event.data.call_id,
				value
			},
			client
		);
	}

	async error(
		services: Services,
		event: MessageEvent,
		value: any,
		client?: MessageEventSource | null
	) {
		return this.send(
			services,
			event,
			{
				type: `${event.data.type}/error`,
				call_id: event.data.call_id,
				value
			},
			client
		);
	}

	heartbeat(services: Services, event: MessageEvent) {
		this.success(services, event, { state: 'up' }, event.source);
	}

	async onMessage(services: Services, event: MessageEvent) {
		services.telemetry.logger.info('[com#onMessage] Message received', {
			type: event.data.type,
			call_id: event.data.call_id
		});

		try {
			if (event.data.type === 'heartbeat') {
				return this.heartbeat(services, event);
			}

			this.emit(event.data.type, services, event);
		} catch (err) {
			console.error(err);

			await this.send(
				services,
				event,
				{
					call_id: event.data.call_id,
					type: `${event.data.type}/error`,
					error: err
				},
				event.source!
			);
		}
	}
}
