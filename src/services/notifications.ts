import { get } from 'svelte/store';

import * as stores from './stores';

const VAPID_PUBLIC_KEY =
	'BAgwe9ysA8s82wHThn0Y9-a_ASp-ajnLHP31x9EYJOeMJbp4LG1rZWNG5t1bznaKLgUVr3qtHDYyfdqzUA14-dY';

/**
 * @see https://github.com/mdn/serviceworker-cookbook/blob/master/tools.js#L4C1-L17C2
 */
function urlBase64ToUint8Array(base64String: string): Uint8Array {
	var padding = '='.repeat((4 - (base64String.length % 4)) % 4);
	var base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');

	var rawData = window.atob(base64);
	var outputArray = new Uint8Array(rawData.length);

	for (var i = 0; i < rawData.length; ++i) {
		outputArray[i] = rawData.charCodeAt(i);
	}
	return outputArray;
}

export async function subscribePushNotifications() {
	const subscription = get(stores.pushSubscription);

	if (subscription !== null) {
		return subscription;
	}

	return navigator.serviceWorker.ready.then(function (registration) {
		registration.pushManager
			.subscribe({
				userVisibleOnly: true,
				applicationServerKey: urlBase64ToUint8Array(VAPID_PUBLIC_KEY)
			})
			.then(function (subscription) {
				stores.pushSubscription.set(JSON.parse(JSON.stringify(subscription)));

				console.log(
					'Subscribed for push:',
					subscription.endpoint,
					subscription.getKey('p256dh'),
					subscription.getKey('auth'),
					JSON.stringify(subscription)
				);

				return subscription;
			})
			.catch(function (error) {
				console.log('Subscription failed:', error);
			});
	});
}

export async function requestPermission() {
	Notification.requestPermission().then(function (permission) {
		if (permission === 'granted') {
			console.log('Notification permission granted.');

			subscribePushNotifications();
		} else {
			console.log('Unable to get permission to notify.');
		}
	});
}
