import type { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
	webServer: {
		command: 'npm run preview',
		url: 'http://localhost:5173'
	},
	reporter: [['list'], ['html']],
	testDir: 'tests',
	testMatch: /(.+\.)?(test|spec)\.[jt]s/,
	timeout: 30_000,

	use: {
		baseURL: process.env.BASE_URL || 'http://localhost:5173'
	}
};

export default config;
