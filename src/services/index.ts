import type { Logger } from '../common/logger';
import type { ModelConfig } from '@getanthill/datastore/dist/typings';

import type { Account, Conversation, Membership, Message, Position } from '../types';

import Datastore from '@getanthill/datastore/dist/sdk/Datastore';
import Model from '@getanthill/datastore/dist/sdk/Model';

import { build as buildLogger } from '../common/logger';

import * as communications from './communications';
import * as data from './data';
import * as stores from './stores';
import * as permissions from './permissions';
import * as notifications from './notifications';
import * as geolocation from './geolocation';
import * as shares from './shares';
import * as screen from './screen';

import { get } from 'svelte/store';

export interface Services {
	telemetry: {
		logger: Logger;
	};
	datastores: {
		models: Datastore;
	};
	models: {
		accounts: Model<Account>;
		conversations: Model<Conversation>;
		memberships: Model<Membership>;
		messages: Model<Message>;
		positions: Model<Position>;
	};
	communications: typeof communications;
	data: typeof data;
	stores: typeof stores;
	notifications: typeof notifications;
	geolocation: typeof geolocation;
	permissions: typeof permissions;
	shares: typeof shares;
	screen: typeof screen;
}

export async function registerAccessToken(
	baseUrl: string,
	searchParams: URLSearchParams
): Promise<void> {
	if (searchParams.has('token')) {
		const token = searchParams.get('token') as string;
		const client = new Datastore({
			baseUrl,
			token,
			connector: 'http',
			timeout: 60_000
		});

		await client.core.request({
			method: 'post',
			url: '/auth',
			data: {
				token
			}
		});
	}
}

export async function getModels(datastore: Datastore): Promise<Record<string, ModelConfig>> {
	console.log('[services#build] Getting available models');

	let models = get(stores.models) ?? ((await data.g('models')) as Record<string, ModelConfig>);

	if (!models) {
		const { data: _models } = await datastore.getModels();

		models = _models;
	}

	await data.s('models', models);

	return models;
}

export function log(services: Services, str: string, context: any = {}) {
	services.telemetry.logger.info(str);
	const lines = get(services.stores.logs);

	services.stores.logs.set([...lines, `${str}: ${JSON.stringify(context)}`]);
}

export async function waitForServiceWorker(services: Services) {
	registerServiceWorker();

	try {
		log(services, '[services#init] Waiting for service worker...');
		await services.communications.heartbeat();
	} catch (err) {
		services.communications.setReady(false);

		// log(services, '[services#init] Getting service worker registrations...');
		// const registrations = await navigator.serviceWorker.getRegistrations();

		// log(services, '[services#init] Unregistering service workers...');
		// for (const registration of registrations) {
		// 	await registration.unregister();
		// }

		// log(services, '[services#init] Register main service worker...');
		// navigator.serviceWorker.register('./service-worker.js', { type: 'module' });

		await services.communications.send({ type: 'init' });

		log(services, '[services#init] Waiting for service worker...');
		await services.communications.heartbeat();
	}
}

export async function init(services: Services) {
	services.stores.logs.set([]);

	if (Object.keys(services.models).length > 0) {
		// Already initialized;
		return;
	}

	log(services, '[services#init] Starting the services initialization...');

	log(services, '[services#init] Waiting for the service worker...');
	await waitForServiceWorker(services);

	// Datastore initialization
	const searchParams = new URLSearchParams(window.location.search);

	log(services, '[services#init] Getting ds URL...');
	const baseUrl = await data.g('datastore', 'http://localhost:3001');
	log(services, '[services#init] Datastore base URL', baseUrl);

	services.datastores = {
		models: new Datastore({
			baseUrl,
			token: '<cookie>',
			connector: 'http',
			timeout: 60_000
		})
	};

	services.stores.datastore.set(services.datastores.models);

	log(services, '[services#init] Setting datastore URL', baseUrl);
	await data.s('datastore', baseUrl).catch((err) => console.error(err));

	log(services, '[services#init] Requesting data persistence...');
	services.data.requestPermission();

	log(services, '[services#init] Registering access token...');
	await registerAccessToken(baseUrl, searchParams);

	log(services, '[services#init] Retrieving models...');
	const models = await getModels(services.datastores.models);

	log(services, '[services#init] Setting models...');
	services.models.accounts = new Model<Account>(services.datastores.models, models.accounts);
	services.models.conversations = new Model<Conversation>(
		services.datastores.models,
		models.conversations
	);
	services.models.memberships = new Model<Membership>(
		services.datastores.models,
		models.memberships
	);
	services.models.messages = new Model<Message>(services.datastores.models, models.messages);
	services.models.positions = new Model<Position>(services.datastores.models, models.positions);
}

export function registerServiceWorker() {
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.register('./service-worker.js', { type: 'module' });
		addEventListener('load', function () {
			navigator.serviceWorker.register('./service-worker.js', { type: 'module' });
		});
	}
}

registerServiceWorker();
export default function build(): Services {
	const logger = buildLogger();

	// Bind services
	communications.bind();

	const datastores = {
		models: new Datastore({
			baseUrl: 'http://localhost:3001',
			token: '<cookie>',
			connector: 'http',
			timeout: 60_000
		})
	};

	return {
		telemetry: {
			logger
		},
		communications,
		data,
		stores,
		notifications,
		geolocation,
		permissions,
		shares,
		screen,
		datastores,
		// @ts-ignore
		models: {
			// accounts: new Model(datastores.models, models.accounts),
			// conversations: new Model(datastores.models, models.conversations),
			// memberships: new Model(datastores.models, models.memberships),
			// messages: new Model(datastores.models, models.messages),
			// positions: new Model(datastores.models, models.positions)
		}
	};
}
