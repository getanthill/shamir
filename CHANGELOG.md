## [1.5.18](https://gitlab.com/getanthill/shamir/compare/v1.5.17...v1.5.18) (2024-03-26)


### Bug Fixes

* unsubscribe positions ([6e58c6a](https://gitlab.com/getanthill/shamir/commit/6e58c6af1ad4704b43c07ea585add6453083758b))

## [1.5.17](https://gitlab.com/getanthill/shamir/compare/v1.5.16...v1.5.17) (2024-03-26)


### Bug Fixes

* improve service worker registration ([1873ae2](https://gitlab.com/getanthill/shamir/commit/1873ae289c8f84a05a9e6d887d5fd74954cfc899))

## [1.5.16](https://gitlab.com/getanthill/shamir/compare/v1.5.15...v1.5.16) (2024-03-13)


### Bug Fixes

* lower the heartbeat delay ([53bc8b7](https://gitlab.com/getanthill/shamir/commit/53bc8b761befeba0bae3e48bbb5fe4fd4fd97a61))
* try without unregister and register  step ([d8ba7e3](https://gitlab.com/getanthill/shamir/commit/d8ba7e391a5c39f2b0f96576d79551f62af76060))

## [1.5.15](https://gitlab.com/getanthill/shamir/compare/v1.5.14...v1.5.15) (2024-03-13)


### Bug Fixes

* allows to call main from message ([b054c5b](https://gitlab.com/getanthill/shamir/commit/b054c5b29492a41edf08396364d6c62cd8fde4f5))

## [1.5.14](https://gitlab.com/getanthill/shamir/compare/v1.5.13...v1.5.14) (2024-03-13)


### Bug Fixes

* try to force unregister the service worker in case of zombie ([fa15d55](https://gitlab.com/getanthill/shamir/commit/fa15d55e322f4150e544ef62b3796a6499e91c40))

## [1.5.13](https://gitlab.com/getanthill/shamir/compare/v1.5.12...v1.5.13) (2024-03-13)


### Bug Fixes

* display number of active service worker in case of init error ([129cca2](https://gitlab.com/getanthill/shamir/commit/129cca2593e78943e6b22f19b02c5ccd13506f63))

## [1.5.12](https://gitlab.com/getanthill/shamir/compare/v1.5.11...v1.5.12) (2024-03-13)


### Bug Fixes

* enforce service worker registration / activation on load ([5bd0e76](https://gitlab.com/getanthill/shamir/commit/5bd0e767d010878aeeba1455536a5d6c24fde847))

## [1.5.11](https://gitlab.com/getanthill/shamir/compare/v1.5.10...v1.5.11) (2024-03-13)


### Bug Fixes

* change the order of parameters in retryable calls ([30a039c](https://gitlab.com/getanthill/shamir/commit/30a039ce1307e2ae5a3e567a1f438919b961175b))
* improve initialization ([2705132](https://gitlab.com/getanthill/shamir/commit/2705132688c738dab2833d7c6a12c28a87eb97f1))
* improve services initialization error log printing ([77114c8](https://gitlab.com/getanthill/shamir/commit/77114c8c509d4aa990c4aabb1529b6804d1b0e4b))
* make the heartbeat call retryable ([de8ac57](https://gitlab.com/getanthill/shamir/commit/de8ac578b071b61c0a66dc52761fe647d83d58bf))
* trigger new version ([14d4b67](https://gitlab.com/getanthill/shamir/commit/14d4b674e0abf56e663b9b8fa7037cb42914b9ce))

## [1.5.11](https://gitlab.com/getanthill/shamir/compare/v1.5.10...v1.5.11) (2024-03-13)


### Bug Fixes

* improve initialization ([2705132](https://gitlab.com/getanthill/shamir/commit/2705132688c738dab2833d7c6a12c28a87eb97f1))

## [1.5.10](https://gitlab.com/getanthill/shamir/compare/v1.5.9...v1.5.10) (2024-03-12)


### Bug Fixes

* allow to not persist ds url on init ([8c43d62](https://gitlab.com/getanthill/shamir/commit/8c43d62534ad293c9342c4239e83d885c11a3370))

## [1.5.9](https://gitlab.com/getanthill/shamir/compare/v1.5.8...v1.5.9) (2024-03-12)


### Bug Fixes

* show bottom app bar on load ([46b237b](https://gitlab.com/getanthill/shamir/commit/46b237b714bbd4d91a331c469cb81ca622fbaff6))

## [1.5.8](https://gitlab.com/getanthill/shamir/compare/v1.5.7...v1.5.8) (2024-03-12)


### Bug Fixes

* improve service worker ready wait ([f297965](https://gitlab.com/getanthill/shamir/commit/f2979657ea28bccbe191765e9810e2ea4d154968))

## [1.5.7](https://gitlab.com/getanthill/shamir/compare/v1.5.6...v1.5.7) (2024-03-12)


### Bug Fixes

* wait for the service worker controller to be effectively ready ([dc734e7](https://gitlab.com/getanthill/shamir/commit/dc734e775e435ba7e4fd6be69276203cd616c114))

## [1.5.6](https://gitlab.com/getanthill/shamir/compare/v1.5.5...v1.5.6) (2024-03-09)


### Bug Fixes

* remove concurrent access to the indexedDb ([87e626f](https://gitlab.com/getanthill/shamir/commit/87e626f6c1cc518293f362b048c5f893fefe0b5e))

## [1.5.5](https://gitlab.com/getanthill/shamir/compare/v1.5.4...v1.5.5) (2024-03-09)


### Bug Fixes

* wait for service worker registrations ([569a48a](https://gitlab.com/getanthill/shamir/commit/569a48a72fe3056f97d13678e2cc9103025a58e2))

## [1.5.4](https://gitlab.com/getanthill/shamir/compare/v1.5.3...v1.5.4) (2024-03-09)


### Bug Fixes

* make service worker immediately available ([137b375](https://gitlab.com/getanthill/shamir/commit/137b37540493dbbcf259602954529a669a3de052))

## [1.5.3](https://gitlab.com/getanthill/shamir/compare/v1.5.2...v1.5.3) (2024-03-09)


### Bug Fixes

* remove incompatible browser logger ([3fefdff](https://gitlab.com/getanthill/shamir/commit/3fefdff58a1f8368a8dd1868e1b6f5d4f116c37a))

## [1.5.2](https://gitlab.com/getanthill/shamir/compare/v1.5.1...v1.5.2) (2024-03-09)


### Bug Fixes

* use of telemetry logger ([3b257bc](https://gitlab.com/getanthill/shamir/commit/3b257bc3688305b780a620c9257554e8eac778b5))

## [1.5.1](https://gitlab.com/getanthill/shamir/compare/v1.5.0...v1.5.1) (2024-03-09)


### Bug Fixes

* improve overall initialization step ([91d2d0e](https://gitlab.com/getanthill/shamir/commit/91d2d0e51088a059a4965a919b3349997d6649d3))

# [1.5.0](https://gitlab.com/getanthill/shamir/compare/v1.4.5...v1.5.0) (2024-03-09)


### Features

* major refactoring of the async communication with service worker ([f58c27a](https://gitlab.com/getanthill/shamir/commit/f58c27aea3c163baa7619fb29582c89cab4eacc0))

## [1.4.5](https://gitlab.com/getanthill/shamir/compare/v1.4.4...v1.4.5) (2024-03-09)


### Bug Fixes

* remove extra processing from axios ([2b7dbef](https://gitlab.com/getanthill/shamir/commit/2b7dbefe2482d9c343471a1351037145ce36b8cd))

## [1.4.4](https://gitlab.com/getanthill/shamir/compare/v1.4.3...v1.4.4) (2024-03-09)


### Bug Fixes

* measure ping to server ([c357375](https://gitlab.com/getanthill/shamir/commit/c357375c737ea1be57510e8155754e26f703bdb7))

## [1.4.3](https://gitlab.com/getanthill/shamir/compare/v1.4.2...v1.4.3) (2024-03-06)


### Bug Fixes

* update grid display to avoid overlapping ([e9fe638](https://gitlab.com/getanthill/shamir/commit/e9fe638df5cf04b6e1d085b2f5a5ae3acfd6af1e))

## [1.4.2](https://gitlab.com/getanthill/shamir/compare/v1.4.1...v1.4.2) (2024-03-06)


### Bug Fixes

* remove metrics without a correct accuracy ([c0562f5](https://gitlab.com/getanthill/shamir/commit/c0562f5f8cace574734e307aad493af8c9b65c68))
* retrieve latest updates first ([e941c29](https://gitlab.com/getanthill/shamir/commit/e941c29fb0c99019342038cf5e7cd59944998e62))

## [1.4.1](https://gitlab.com/getanthill/shamir/compare/v1.4.0...v1.4.1) (2024-03-05)


### Bug Fixes

* use simpler h3 index query ([98de94a](https://gitlab.com/getanthill/shamir/commit/98de94a9d815b543371c31d72c5049ee3acdeba4))

# [1.4.0](https://gitlab.com/getanthill/shamir/compare/v1.3.33...v1.4.0) (2024-03-04)


### Features

* poc WebRTC ([1704f76](https://gitlab.com/getanthill/shamir/commit/1704f7634a2582d1b740b123b1370e986aa27acc))

## [1.3.33](https://gitlab.com/getanthill/shamir/compare/v1.3.32...v1.3.33) (2024-03-04)


### Bug Fixes

* improve loading flow ([ea0647b](https://gitlab.com/getanthill/shamir/commit/ea0647b754941cab7ad3a7f8d077daab4428a803))

## [1.3.32](https://gitlab.com/getanthill/shamir/compare/v1.3.31...v1.3.32) (2024-03-04)


### Bug Fixes

* improve auth flow in map page ([766f5a7](https://gitlab.com/getanthill/shamir/commit/766f5a72098ca86487354d8081e47b26e094d6d5))

## [1.3.31](https://gitlab.com/getanthill/shamir/compare/v1.3.30...v1.3.31) (2024-03-03)


### Bug Fixes

* show points on map with accuracy ([ad50ea3](https://gitlab.com/getanthill/shamir/commit/ad50ea31193aa5f07cdb7108411d31407eac293e))

## [1.3.30](https://gitlab.com/getanthill/shamir/compare/v1.3.29...v1.3.30) (2024-03-03)


### Bug Fixes

* allows to sync position ([add3c1a](https://gitlab.com/getanthill/shamir/commit/add3c1a1642e86959296fe2c4029cf277b840538))

## [1.3.29](https://gitlab.com/getanthill/shamir/compare/v1.3.28...v1.3.29) (2024-03-03)


### Bug Fixes

* decrease the angle to find cells ([b7767c4](https://gitlab.com/getanthill/shamir/commit/b7767c44cb1241fba20cb84c8670e0fc6c7a9ced))

## [1.3.28](https://gitlab.com/getanthill/shamir/compare/v1.3.27...v1.3.28) (2024-03-03)


### Bug Fixes

* implements h3 geolocation indexing ([19ba24a](https://gitlab.com/getanthill/shamir/commit/19ba24a9fb14215f6bc943606d747703d3ad8d46))

## [1.3.27](https://gitlab.com/getanthill/shamir/compare/v1.3.26...v1.3.27) (2024-03-03)


### Bug Fixes

* disable authentication on otp cancellation ([20f0133](https://gitlab.com/getanthill/shamir/commit/20f013380ce561ea1ccbc25cb7956ca1c9517f91))

## [1.3.26](https://gitlab.com/getanthill/shamir/compare/v1.3.25...v1.3.26) (2024-03-03)


### Bug Fixes

* implements Webauthn local storage layer ([f8f9058](https://gitlab.com/getanthill/shamir/commit/f8f90587280177a5bd8d76ff2c8fc4c11363fc46))

## [1.3.25](https://gitlab.com/getanthill/shamir/compare/v1.3.24...v1.3.25) (2024-03-02)


### Bug Fixes

* define geolocation options ([0af3c3a](https://gitlab.com/getanthill/shamir/commit/0af3c3af551518787e6f12a227d4c5904c4ea51f))

## [1.3.24](https://gitlab.com/getanthill/shamir/compare/v1.3.23...v1.3.24) (2024-03-02)


### Bug Fixes

* overflow handled on qrcode page ([f54efbe](https://gitlab.com/getanthill/shamir/commit/f54efbecb03539fb600ff611bde253741abfd0f7))

## [1.3.23](https://gitlab.com/getanthill/shamir/compare/v1.3.22...v1.3.23) (2024-03-02)


### Bug Fixes

* handles keyboard on mobile correctly with bottom app bar ([e3bfaf6](https://gitlab.com/getanthill/shamir/commit/e3bfaf60b2f4c9a419f806e10112964ba6ac381b))

## [1.3.22](https://gitlab.com/getanthill/shamir/compare/v1.3.21...v1.3.22) (2024-03-02)


### Bug Fixes

* try implementation of a bottom app bar ([0d30707](https://gitlab.com/getanthill/shamir/commit/0d30707cc2310bb4ea49c06cdf8073e9f919f6bb))

## [1.3.21](https://gitlab.com/getanthill/shamir/compare/v1.3.20...v1.3.21) (2024-03-02)


### Bug Fixes

* adding status badges ([0697711](https://gitlab.com/getanthill/shamir/commit/06977119c4ea47443bd4a8636067ba543c2180d2))

## [1.3.20](https://gitlab.com/getanthill/shamir/compare/v1.3.19...v1.3.20) (2024-03-02)


### Bug Fixes

* adding find logic ([0c3fad8](https://gitlab.com/getanthill/shamir/commit/0c3fad817dbe618c1f85056cd6cc4f6c01d6e129))

## [1.3.19](https://gitlab.com/getanthill/shamir/compare/v1.3.18...v1.3.19) (2024-03-02)


### Bug Fixes

* implements data storage with service worker communication ([f924d83](https://gitlab.com/getanthill/shamir/commit/f924d836b876cba326abcd7ac1b9f1cdec4c0438))

## [1.3.18](https://gitlab.com/getanthill/shamir/compare/v1.3.17...v1.3.18) (2024-03-01)


### Bug Fixes

* import smui css ([2ebe811](https://gitlab.com/getanthill/shamir/commit/2ebe811996d33468a94d6a80334018af62ed8f6b))

## [1.3.17](https://gitlab.com/getanthill/shamir/compare/v1.3.16...v1.3.17) (2024-02-29)


### Bug Fixes

* supports url navigation ([5364705](https://gitlab.com/getanthill/shamir/commit/53647059f157e796162ba28a7907de1d8bf75d9c))

## [1.3.16](https://gitlab.com/getanthill/shamir/compare/v1.3.15...v1.3.16) (2024-02-29)


### Bug Fixes

* update manifest ([61f3dfd](https://gitlab.com/getanthill/shamir/commit/61f3dfdd9be44c488ee1e46971e53edee5a0a9b9))

## [1.3.15](https://gitlab.com/getanthill/shamir/compare/v1.3.14...v1.3.15) (2024-02-29)


### Bug Fixes

* add general settings page ([b02be57](https://gitlab.com/getanthill/shamir/commit/b02be57422b9cec7d04689fb11082840b3debe93))

## [1.3.14](https://gitlab.com/getanthill/shamir/compare/v1.3.13...v1.3.14) (2024-02-29)


### Bug Fixes

* lock screen on map ([246e124](https://gitlab.com/getanthill/shamir/commit/246e1242a495feaa4c2460bc4f9ecccf765496d2))

## [1.3.13](https://gitlab.com/getanthill/shamir/compare/v1.3.12...v1.3.13) (2024-02-29)


### Bug Fixes

* **images:** adding poc on qrcode and images capture ([a628666](https://gitlab.com/getanthill/shamir/commit/a6286661853b4c55f4a04033dc54c1697daa016c))

## [1.3.12](https://gitlab.com/getanthill/shamir/compare/v1.3.11...v1.3.12) (2024-02-29)


### Bug Fixes

* try the background implementation of geolocation in background ([bea33fd](https://gitlab.com/getanthill/shamir/commit/bea33fdf650e2cc013ae74d52752f7b6d515f329))
* update location display in leaflet ([062d719](https://gitlab.com/getanthill/shamir/commit/062d71928d63bf2c24331c15f3f157d651baaad9))

## [1.3.11](https://gitlab.com/getanthill/shamir/compare/v1.3.10...v1.3.11) (2024-02-28)


### Bug Fixes

* improve UI for geolocation ([d5e5b79](https://gitlab.com/getanthill/shamir/commit/d5e5b79d67211364dbea0bf5a4aa33e9ec335a51))

## [1.3.10](https://gitlab.com/getanthill/shamir/compare/v1.3.9...v1.3.10) (2024-02-28)


### Bug Fixes

* reset default zomm level ([af94ed7](https://gitlab.com/getanthill/shamir/commit/af94ed7bca471afcf30e141a3e0e060780ba6e00))
* update current market instead of recreating it ([62d5148](https://gitlab.com/getanthill/shamir/commit/62d51485cf51c8e3f66edd19a8e9ea4bed3d7352))
* use default zoom level on update position ([04d46ed](https://gitlab.com/getanthill/shamir/commit/04d46eda03a2ae68733454cb8a1692ccabaedcb2))

## [1.3.9](https://gitlab.com/getanthill/shamir/compare/v1.3.8...v1.3.9) (2024-02-28)


### Bug Fixes

* test geolocation ([2ebad0d](https://gitlab.com/getanthill/shamir/commit/2ebad0dd9f176d3860e2379aa9f22f64b3cc5e2a))

## [1.3.8](https://gitlab.com/getanthill/shamir/compare/v1.3.7...v1.3.8) (2024-02-27)


### Bug Fixes

* **dep:** update  dependencies ([ef55216](https://gitlab.com/getanthill/shamir/commit/ef55216cf3a069600cfd6589e590a3f7c22b4f64))

## [1.3.7](https://gitlab.com/getanthill/shamir/compare/v1.3.6...v1.3.7) (2024-01-31)


### Bug Fixes

* remove fingerprint from personal encryption token ([3a058bc](https://gitlab.com/getanthill/shamir/commit/3a058bc078e4b4a1170bd31ebbd06a764e1ac686))

## [1.3.6](https://gitlab.com/getanthill/shamir/compare/v1.3.5...v1.3.6) (2024-01-26)


### Bug Fixes

* fix flex display on text field ([4da8c5b](https://gitlab.com/getanthill/shamir/commit/4da8c5b4a27983a443b2aef6d6fb4e2e58ccc78c))

## [1.3.5](https://gitlab.com/getanthill/shamir/compare/v1.3.4...v1.3.5) (2024-01-26)


### Bug Fixes

* **ui:** improve responsive layout ([98ef372](https://gitlab.com/getanthill/shamir/commit/98ef37222887fca4a274a72e60d19614d638d9eb))

## [1.3.4](https://gitlab.com/getanthill/shamir/compare/v1.3.3...v1.3.4) (2024-01-26)


### Bug Fixes

* disable light theme ([5999be3](https://gitlab.com/getanthill/shamir/commit/5999be316a8e4025168328ae06e201da607f2886))

## [1.3.3](https://gitlab.com/getanthill/shamir/compare/v1.3.2...v1.3.3) (2024-01-26)


### Bug Fixes

* **ci:** fix deploy pages ([8b07f53](https://gitlab.com/getanthill/shamir/commit/8b07f533651c979ca4b9d5a06594e0a36e6140ce))

## [1.3.2](https://gitlab.com/getanthill/shamir/compare/v1.3.1...v1.3.2) (2024-01-26)


### Bug Fixes

* **ci:** fix build after semantic release ([646429b](https://gitlab.com/getanthill/shamir/commit/646429bc44e9bacbb8a44b9a54117867500dee55))

## [1.3.1](https://gitlab.com/getanthill/shamir/compare/v1.3.0...v1.3.1) (2024-01-26)


### Bug Fixes

* **debug:** check values differences ([49d4381](https://gitlab.com/getanthill/shamir/commit/49d43817a858b3d83551630a816a6a09d8eb8b0e))

# [1.3.0](https://gitlab.com/getanthill/shamir/compare/v1.2.8...v1.3.0) (2024-01-26)


### Features

* lighthouse ci ([2c0d57d](https://gitlab.com/getanthill/shamir/commit/2c0d57d2c71bd3e92eda5dc41b25f8480c86847a))

## [1.2.8](https://gitlab.com/getanthill/shamir/compare/v1.2.7...v1.2.8) (2024-01-23)


### Bug Fixes

* support missing search params from $page url ([fb083e6](https://gitlab.com/getanthill/shamir/commit/fb083e6de38160cdd0009488e9c8f881aeb6a32b))

## [1.2.7](https://gitlab.com/getanthill/shamir/compare/v1.2.6...v1.2.7) (2024-01-23)


### Bug Fixes

* update model to support auth token change ([61bd32f](https://gitlab.com/getanthill/shamir/commit/61bd32f018100f82a478bfb2322d968a76d420b7))

## [1.2.6](https://gitlab.com/getanthill/shamir/compare/v1.2.5...v1.2.6) (2024-01-22)


### Bug Fixes

* replacement of browser fingerprint library ([5acb2b3](https://gitlab.com/getanthill/shamir/commit/5acb2b389b467548e9ebea128108e2b2da546f9b))

## [1.2.5](https://gitlab.com/getanthill/shamir/compare/v1.2.4...v1.2.5) (2024-01-22)


### Bug Fixes

* move clear timeout after auth refresh ([f1b9978](https://gitlab.com/getanthill/shamir/commit/f1b9978b56c0d305585e1d6b494dd0ef23b3c77a))
* wait for heartbeat ([901ac2b](https://gitlab.com/getanthill/shamir/commit/901ac2b2f310c5fe499ca606d96b1b7b7562e021))

## [1.2.4](https://gitlab.com/getanthill/shamir/compare/v1.2.3...v1.2.4) (2024-01-22)


### Bug Fixes

* supports keep me in on signin ([14274a1](https://gitlab.com/getanthill/shamir/commit/14274a189cc5f00fd5b5a690a5e4a8bcb962737c))

## [1.2.3](https://gitlab.com/getanthill/shamir/compare/v1.2.2...v1.2.3) (2024-01-20)


### Bug Fixes

* disable autofill background color ([7a438e4](https://gitlab.com/getanthill/shamir/commit/7a438e4b3cabcb463dedd9d8b4fb32bd3995695b))

## [1.2.2](https://gitlab.com/getanthill/shamir/compare/v1.2.1...v1.2.2) (2024-01-20)


### Bug Fixes

* handle autocomplete in signup ([bb8ba2e](https://gitlab.com/getanthill/shamir/commit/bb8ba2eea76e35954c035f4b4dd814a81d1b888f))

## [1.2.1](https://gitlab.com/getanthill/shamir/compare/v1.2.0...v1.2.1) (2024-01-20)


### Bug Fixes

* hide paper during ini ([4b3d276](https://gitlab.com/getanthill/shamir/commit/4b3d276240218166e7a85b3830a09f07e50236a5))

# [1.2.0](https://gitlab.com/getanthill/shamir/compare/v1.1.2...v1.2.0) (2024-01-20)


### Features

* allows to signup ([bd26383](https://gitlab.com/getanthill/shamir/commit/bd26383c9bb9a0875f11c815375a929da80ab943))

## [1.1.2](https://gitlab.com/getanthill/shamir/compare/v1.1.1...v1.1.2) (2024-01-20)


### Bug Fixes

* use device fingerprint as default OTP ([44150c9](https://gitlab.com/getanthill/shamir/commit/44150c97347f6ef3d31dafff2338d7029956723a))

## [1.1.1](https://gitlab.com/getanthill/shamir/compare/v1.1.0...v1.1.1) (2024-01-20)


### Bug Fixes

* support query parameter ([ffef400](https://gitlab.com/getanthill/shamir/commit/ffef40093f84a2bf13ad654bcd179b132ce4d3e4))

# [1.1.0](https://gitlab.com/getanthill/shamir/compare/v1.0.3...v1.1.0) (2024-01-20)


### Features

* allows to redirect user after a valid signin ([3a453bb](https://gitlab.com/getanthill/shamir/commit/3a453bb2728ec3b54b794802d949ad9669ac03b5))

## [1.0.3](https://gitlab.com/getanthill/shamir/compare/v1.0.2...v1.0.3) (2024-01-20)


### Bug Fixes

* update semantic plugin ([d3e461b](https://gitlab.com/getanthill/shamir/commit/d3e461ba6e2c4b7ef026b7c9b0702db330d4bad7))

## [1.0.2](https://gitlab.com/getanthill/shamir/compare/v1.0.1...v1.0.2) (2024-01-20)


### Bug Fixes

* block signup on signin ([45d6779](https://gitlab.com/getanthill/shamir/commit/45d6779a2ff363534809c0e6a92d17bfd9e9215e))

## [1.0.1](https://gitlab.com/getanthill/shamir/compare/v1.0.0...v1.0.1) (2024-01-20)


### Bug Fixes

* **auth:** clean sid cookie ([118bf85](https://gitlab.com/getanthill/shamir/commit/118bf855ac05b7eb9897aac6d5d4987583da881f))
* **ci:** add package.json in the artifacts ([4fd72b4](https://gitlab.com/getanthill/shamir/commit/4fd72b41dcc0d8b78e69758bf265b2bb8864e863))

# 1.0.0 (2024-01-19)


### Bug Fixes

* add smooth animations to transition elements ([ea44f94](https://gitlab.com/getanthill/shamir/commit/ea44f94a54eaa259a6c446dc6a97a1efecdcff5f))
* **auth:** allows to navigate between pages with authentication ([d4c76be](https://gitlab.com/getanthill/shamir/commit/d4c76beeb17718a0880d5ed1e90126ad3076be73))
* fix threshold update with keyboard ([8faf731](https://gitlab.com/getanthill/shamir/commit/8faf7311da6fdbce71f35865381922076029090c))
* remove account id from template ([01f49bd](https://gitlab.com/getanthill/shamir/commit/01f49bd2aae0ba14aec0d906def52f02bdb7dd84))
* **shamir:** fix redirect on index.html pages ([351ca97](https://gitlab.com/getanthill/shamir/commit/351ca97305f02a03038c1ee0ede3603fc8fee480))
* **signin:** fix flex textfield size ([6ee50bb](https://gitlab.com/getanthill/shamir/commit/6ee50bbe43bf7e50bfd90aa8b13d091d17453a28))
* **signin:** fix prerendering build step ([56b80f3](https://gitlab.com/getanthill/shamir/commit/56b80f33ab3427b4fe251117f6b61e5a38788430))
* update footer with version ([be4c0fb](https://gitlab.com/getanthill/shamir/commit/be4c0fb77ebd5dafe4f25fe03c688c3f11c57f2a))


### Features

* **e2e:** poc e2e chat ([9b32332](https://gitlab.com/getanthill/shamir/commit/9b32332e7221853bb297259906f2b1e91a8d9fed))
* **e2e:** poc e2e flow ([63950e1](https://gitlab.com/getanthill/shamir/commit/63950e1294779cb218e5c5b710dbbc3cf4426058))
* enable Shamir secret sharing client side ([8005c84](https://gitlab.com/getanthill/shamir/commit/8005c84fee12fc1e1184cfdb908cf38d689d73b1))
* first working version ([6ab46ba](https://gitlab.com/getanthill/shamir/commit/6ab46baf589e3f7ddd7dcb774c03d9809bd583cd))
* improve look and feel of the demo signin form ([c5b522f](https://gitlab.com/getanthill/shamir/commit/c5b522f0006d97495c2ddb70432f62fe76ec6c36))
* improve overall UI ([0d560d0](https://gitlab.com/getanthill/shamir/commit/0d560d0ff9ea0842efbe16720a9484d1716ab4bc))
* **smui:** integrates SvelteMaterial UI with theme ([bdadc1b](https://gitlab.com/getanthill/shamir/commit/bdadc1b43f5c43c7ad07564079599afb33dec762))
* **sw:** configures basic service worker ([cfa13de](https://gitlab.com/getanthill/shamir/commit/cfa13de80032c4b55ba747b73ce23520fff6a901))
