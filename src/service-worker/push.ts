import type { Services } from './services';

export async function bind(services: Services) {
	// Listen to `push` notification event. Define the text to be displayed
	// and show the notification.
	self.addEventListener('push', function (event) {
		const payload = JSON.parse(event.data?.text() ?? '{}');
		console.log('Received push notification', event, payload);
		event.waitUntil(self.registration.showNotification(payload.title, payload));
	});

	self.addEventListener('notificationclick', function (event) {
		console.log('[notifications] Clicked on notification', event);
		const url = new URL(event.notification?.data?.url);
		event.notification.close(); // Android needs explicit close.
		event.waitUntil(
			clients.matchAll({ type: 'window' }).then((windowClients) => {
				// Check if there is already a window/tab open with the target URL
				for (var i = 0; i < windowClients.length; i++) {
					var client = windowClients[i];
					const clientUrl = new URL(client.url);
					// If so, just focus it.
					if (clientUrl.host === url.host && 'focus' in client) {
						client.focus();
						client.navigate(url.pathname);
						return;
					}
				}
				// If not, then open the target URL in a new window/tab.
				if (clients.openWindow) {
					return clients.openWindow(url);
				}
			})
		);
	});
}
