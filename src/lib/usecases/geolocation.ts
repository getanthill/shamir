import type { Services } from '../../types';

import { get } from 'svelte/store';

import { v4 as uuidv4 } from 'uuid';
import * as h3 from 'h3-js';

export async function save(services: Services, cell: string | null) {
	if (!cell) {
		return;
	}

	const auth = get(services.stores.auth)?.tokens?.auth;
	if (!auth) {
		return;
	}

	const location = get(services.stores.location);
	if (!location) {
		return;
	}

	const cellResolution = h3.getResolution(cell);
	const cellAtResolution =
		cellResolution > 10
			? h3.cellToParent(cell, 10)
			: cellResolution !== 10
				? h3.cellToCenterChild(cell, 10)
				: cell;

	await services.models.positions.apply(
		'POSITION_VISITED',
		{
			position_id: uuidv4(),
			user_id: auth,
			h3: cellAtResolution,
			latitude: location.coords.latitude,
			longitude: location.coords.longitude,
			accuracy: location.coords.accuracy
		},
		{},
		'1.0.0'
	);
}

export default {};
