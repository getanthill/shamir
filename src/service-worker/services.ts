import type { Logger } from '../common/logger';

import { build as buildLogger } from '../common/logger';

import Communication from './communication';

import * as data from './data';

export interface Services {
	telemetry: {
		logger: Logger;
	};
	communication: Communication;
	data: typeof data;
}

export function build(): Services {
	const logger = buildLogger();

	logger.info('[services#build] Building services...');
	const communication = new Communication();

	return {
		telemetry: {
			logger
		},
		communication,
		data
	};
}

const services = build();

export default services;
