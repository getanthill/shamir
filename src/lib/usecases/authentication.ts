import type { AccountCreated, AuthResponse, Services } from '../../types';

import { get } from 'svelte/store';

import * as sec from '@getanthill/sec';
import { v4 as uuidv4 } from 'uuid';

export async function register(
	email: string,
	password: string,
	additionalTokenIds?: string[],
	namespace?: string
): Promise<{ tokens: Record<string, string>; account: AccountCreated }> {
	const tokens = sec.usecases.crypto.getTokens(email, password, additionalTokenIds, namespace);

	const { privateKey, publicKey } = await sec.usecases.crypto.generateKeyPair();

	const accountData = await sec.usecases.data.encrypt(tokens.encryption, {
		email,
		private_key: privateKey
	});

	const account: AccountCreated = {
		auth: tokens.auth,
		public_key: publicKey,
		data: accountData
	};

	return { tokens, account };
}

export async function signup(
	services: Services,
	email: string,
	password: string,
	keepMeLoggedIn: boolean = true
): Promise<AuthResponse> {
	services.telemetry.logger.info('[authentication#signup] Account signup', {
		email
	});

	const registration = await register(email, password);

	const accountId = uuidv4();
	const tokens = registration.tokens;

	const { data: accountCreated } = await services.datastores.models.apply(
		'accounts',
		accountId,
		'ACCOUNT_CREATED',
		'1.0.0',
		registration.account,
		{}
	);

	const {
		data: [account]
	} = await services.datastores.models.decrypt('accounts', [accountCreated]);

	return storeAuth(services, { tokens, account }, keepMeLoggedIn);
}

export async function signin(
	services: Services,
	email: string,
	password: string,
	keepMeLoggedIn: boolean = true
): Promise<AuthResponse> {
	services.telemetry.logger.info('[authentication#signup] Account signin', {
		email
	});

	const registration = await register(email, password);

	const tokens = registration.tokens;
	let account;

	const {
		data: [_myAccount]
	} = await services.datastores.models.find(
		'accounts',
		{
			auth: tokens.auth
		},
		0,
		1,
		{
			// @ts-ignore
			decrypt: true
		}
	);

	if (!_myAccount) {
		return signoff(services);
	} else {
		account = _myAccount;
	}

	return storeAuth(services, { tokens, account }, keepMeLoggedIn);
}

export async function getSid(services: Services): Promise<string> {
	const {
		data: { sid }
	} = await services.datastores.models.core.request({
		method: 'get',
		url: '/auth',
		params: {
			cookies: ['sid']
		}
	});

	return sid;
}

export async function setSid(services: Services, sid: string) {
	return services.datastores.models.core.request({
		method: 'post',
		url: '/auth',
		data: {
			sid
		}
	});
}

export function getOtp(services: Services, defaultOtp: string = ''): Promise<string> {
	let otp = get(services.stores.otp);
	if (otp) {
		return Promise.resolve(otp);
	}

	return new Promise((resolve) => {
		services.stores.requestOtp.set(true);

		services.stores.requestOtp.subscribe((value) => {
			if (value === true) {
				return;
			}

			otp = get(services.stores.otp) || defaultOtp;

			return resolve(otp);
		});
	});
}

export async function storeAuth(
	services: Services,
	auth: AuthResponse,
	keepMeLoggedIn: boolean = true
): Promise<AuthResponse> {
	if (keepMeLoggedIn === false) {
		return auth;
	}

	if (auth.account === null) {
		return auth;
	}

	const otp = await getUniqueOTP(services);

	// Keep the OTP valid for a short laps of time
	sessionStorage.setItem('otp', otp);
	setTimeoutOnRemoveOtp();

	if (otp === '') {
		return signoff(services);
	}

	const sid = uuidv4();

	const { encryption: encryptionKey } = await sec.getTokens(sid, otp);
	setSid(services, sid).catch((err) => {
		console.error(err);
	});

	const encryptedAuthString = await sec.usecases.data.encrypt(encryptionKey, auth);

	services.data.update('local', {
		name: 'auth',
		value: encryptedAuthString
	});
	services.stores.auth.set(auth);
	services.stores.otp.set(otp);

	return auth;
}

export async function signoff(services: Services): Promise<AuthResponse> {
	const auth = {
		tokens: {},
		account: null
	};

	services.stores.auth.set(auth);
	services.stores.otp.set('');
	sessionStorage.removeItem('otp');
	services.data.remove('local', 'auth');
	services.data.remove('local', 'otp');
	services.data.remove('local', 'webauthn');

	getSid(services).catch((err) => {
		console.error(err);
	});

	return auth;
}

let removeOtpTimeout: NodeJS.Timeout;
function setTimeoutOnRemoveOtp(delay = 10 * 60 * 1000) {
	clearTimeout(removeOtpTimeout);

	removeOtpTimeout = setTimeout(() => sessionStorage.removeItem('otp'), 10 * 60 * 1000);
}

export async function auth(services: Services): Promise<AuthResponse> {
	let authResponse = get(services.stores.auth);

	if (authResponse.account !== null) {
		return storeAuth(services, authResponse);
	}

	const auth = await services.data.g('auth', '');

	if (!auth) {
		return storeAuth(services, authResponse);
	}

	const [sid, otp] = await Promise.all([getSid(services), getUniqueOTP(services)]);

	const { encryption: encryptionKey } = await sec.getTokens(sid, otp);

	// Keep the OTP valid for a short laps of time
	sessionStorage.setItem('otp', otp);
	setTimeoutOnRemoveOtp();

	try {
		const res = await sec.usecases.data.decrypt(encryptionKey, auth);

		return storeAuth(services, res);
	} catch (err) {
		!!sid &&
			setSid(services, sid).catch((err) => {
				console.error(err);
			});

		console.error(err);
	}

	return signoff(services);
}

async function createUniqueOTP(services: Services) {
	const otp = btoa(ab2str(generateRandomChallenge()));

	await services.data.s('otp', otp);

	const credentials = await createPasskey(services, otp);

	if (credentials === null) {
		return getOtp(services);
	}

	const utf8Decoder = new TextDecoder('utf-8');
	const decodedClientData = utf8Decoder.decode(credentials!.response.clientDataJSON);
	const clientDataObj = JSON.parse(decodedClientData);

	return clientDataObj.challenge;
}

async function getUniqueOTP(services: Services) {
	const sessionOtp = sessionStorage.getItem('otp');
	if (sessionOtp) {
		return sessionOtp;
	}

	const otp = await services.data.g('otp');

	if (!otp) {
		return createUniqueOTP(services);
	}

	const credentials = await verifyPasskey(services, otp);

	if (credentials === null) {
		return getOtp(services);
	}

	const utf8Decoder = new TextDecoder('utf-8');
	const decodedClientData = utf8Decoder.decode(credentials.response.clientDataJSON);
	const clientDataObj = JSON.parse(decodedClientData);

	return clientDataObj.challenge;
}

function generateRandomChallenge() {
	let length = 32;
	let randomValues = new Uint8Array(length);
	window.crypto.getRandomValues(randomValues);
	return randomValues;
}

/*
Convert an ArrayBuffer into a string
from https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
*/
function ab2str(buf) {
	return String.fromCharCode.apply(null, new Uint8Array(buf));
}

/*
Convert a string into an ArrayBuffer
from https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
*/
function str2ab(str) {
	const buf = new ArrayBuffer(str.length);
	const bufView = new Uint8Array(buf);
	for (let i = 0, strLen = str.length; i < strLen; i++) {
		bufView[i] = str.charCodeAt(i);
	}
	return buf;
}

export function isWebAuthnSupported(): boolean {
	return Boolean(
		// @ts-ignore
		navigator.credentials && navigator.credentials.create && navigator.credentials.get
	);
}

export async function createPasskey(
	services: Services,
	otp: string,
	email: string = 'john@doe.org'
): Promise<null | PublicKeyCredential> {
	if (isWebAuthnSupported() === false) {
		return null;
	}

	try {
		let credentials = (await navigator.credentials.create({
			publicKey: {
				challenge: str2ab(otp),
				rp: { name: 'shamir', id: window.location.hostname },
				//here you'll want to pass the user's info
				user: { id: new Uint8Array(16), name: email, displayName: email },
				pubKeyCredParams: [
					{ type: 'public-key', alg: -7 },
					{ type: 'public-key', alg: -257 }
				],
				timeout: 60000,
				authenticatorSelection: {
					residentKey: 'preferred',
					requireResidentKey: false,
					userVerification: 'required'
				},
				attestation: 'direct',
				extensions: { credProps: true }
			}
		})) as PublicKeyCredential;

		if (!credentials) {
			return null;
		}

		await services.data.s('webauthn', btoa(ab2str(credentials!.rawId)));

		return credentials;
	} catch (err) {
		return null;
	}
}

export async function verifyPasskey(services: Services, otp: string) {
	if (isWebAuthnSupported() === false) {
		return null;
	}

	const rawId = str2ab(atob(window.rawId ?? (await services.data.g('webauthn'))));
	//to verify a user's credentials, we simply pass the
	//unique ID of the passkey we saved against the user profile
	//in this demo, we just saved it in a global variable

	try {
		let credentials = (await navigator.credentials.get({
			publicKey: {
				challenge: str2ab(otp),
				allowCredentials: [{ type: 'public-key', id: rawId }]
			}
		})) as PublicKeyCredential;

		return credentials;
	} catch (err) {
		return null;
	}
}

export default {};
